<?php
require_once("config.php");

if (
    !empty($_POST['date-meals'] !='') &&
    !empty($_POST['time-meals'] !='') &&
    !empty($_POST['name-meals'] !='') &&
    !empty($_POST['surname-meals'] !='') &&
    !empty($_POST['phone-meals'] !='') &&
    !empty($_POST['email-meals'] !='') &&
    !empty($_POST['number-meals'] !='') &&
    !empty($_POST['id-meals'] !='')
) {
    $date = $conn->real_escape_string($_POST['date-meals']);
    $time = $conn->real_escape_string($_POST['time-meals']);
    $name = $conn->real_escape_string($_POST['name-meals']);
    $surname = $conn->real_escape_string($_POST['surname-meals']);
    $phone = $conn->real_escape_string($_POST['phone-meals']);
    $email = $conn->real_escape_string($_POST['email-meals']);
    $numberMeals = $conn->real_escape_string($_POST['number-meals']);
    $idMeals = $conn->real_escape_string($_POST['id-meals']);

    //Insert Customer if exist
    $sqlCustomers = "INSERT INTO customers (name, surname, phone_number, mail) VALUES ('".$name."','".$surname."','".$phone."','".$email."')";
    $insertCustomer = $conn->query($sqlCustomers);

    $customerSelect = "SELECT c.id AS customers FROM customers AS c
                            WHERE c.mail = '".$email."'";

    $customerQuery= $conn->query($customerSelect);
    $customerId = $customerQuery->fetch_assoc()['customers'];

    if (empty($customerId)){
        echo 'Customer does not exist'; die;
    }

    //Check if Customer book a table during the day
    $customerAlreadyBookedQuery = "SELECT bm.id FROM booking_meals AS bm WHERE bm.id_customer= '".$customerId."' AND bm.date = '".$date."'";
    $customerIdAlreadyBooked = $conn->query($customerAlreadyBookedQuery);

    while($row = $customerIdAlreadyBooked->fetch_assoc()) {
        $customerAlreadyBooked[] = $row;
    }

    if (!empty($customerAlreadyBooked)){
        echo 'Customer already booked a meal for this day'; die;
    }

    $sqlBookingMeals ="INSERT INTO booking_meals (id_customer, number_meals, date, time, id_meals) VALUES 
          ((SELECT id FROM customers WHERE mail='".$email."'),
          '".$numberMeals."',
          '".$date."',
          '".$time."',
          '".$idMeals."')";
    $conn->query($sqlBookingMeals);
    echo 'Meals is cooking !';


}
