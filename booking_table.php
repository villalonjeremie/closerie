<?php
require_once("config.php");

if (
    !empty($_POST['date-table']) &&
    !empty($_POST['time-table']) &&
    !empty($_POST['name-table']) &&
    !empty($_POST['surname-table']) &&
    !empty($_POST['phone-table']) &&
    !empty($_POST['email-table']) &&
    !empty($_POST['number-table']) &&
    !empty($_POST['id-restaurants'])
) {
    //real_escape_string function is for avoiding the error space
    $date = $conn->real_escape_string($_POST['date-table']);
    $time = $conn->real_escape_string($_POST['time-table']);
    $name = $conn->real_escape_string($_POST['name-table']);
    $surname = $conn->real_escape_string($_POST['surname-table']);
    $phone = $conn->real_escape_string($_POST['phone-table']);
    $email = $conn->real_escape_string($_POST['email-table']);
    $numbeSeats = $conn->real_escape_string($_POST['number-table']);
    $idRestaurant = $conn->real_escape_string($_POST['id-restaurants']);

    //set the variable time
    $day = strtolower(date('l', strtotime($date)));
    $dayTimeEnd = $day.'_time_end';
    $dayTimeStart = $day.'_time_start';
    $timeMax = date('H:i', strtotime($time.'+1 hour'));
    $timeMin = date('H:i', strtotime($time.'-1 hour'));

    //Test if Restaurant is opened or not
    $openRestaurant = "SELECT r.id as restaurant_id FROM restaurants AS r
          WHERE r.id = '".$idRestaurant."'
          AND r.".$dayTimeStart." <= '".$timeMin."'
          AND r.".$dayTimeEnd." >= '".$timeMax."'";

    $restaurantQuery = $conn->query($openRestaurant);
    while($row = $restaurantQuery->fetch_assoc()) {
        if(!empty($row['restaurant_id'])) {
            $restaurantId = $row['restaurant_id'];
        }
    }

    if (empty($restaurantId)){
        echo 'No restaurant avalaible'; die;
    }

    //Select Waiter who are free for serving new table
    $waiters = "SELECT e.id AS id, e.max_tables AS max_tables
                    FROM employees AS e WHERE e.id_restaurant = '".$idRestaurant."' AND e.".$day." = 1";

    $waitersWithTable = "SELECT bt.id_employee AS id_employee, count(bt.id_employee) AS c FROM booking_tables AS bt WHERE bt.serving_date='".$date."' GROUP BY id_employee";

    $waitersQuery = $conn->query($waiters);
    $waitersWithTableQuery = $conn->query($waitersWithTable);
    $waitersArray = [];

    while($row = $waitersQuery->fetch_assoc()) {
        $waitersArray[$row['id']] = ['max_tables' => $row['max_tables'],'count' => 0];
    }

    while($row = $waitersWithTableQuery->fetch_assoc()) {
        $waitersArray[$row['id_employee']]['count'] = $row['c'];
    }

    $minC = -1;
    foreach($waitersArray as $k => $v) {
        //Get the waiter who has the minimum number of tables during the day
        if($v['count'] < $v['max_tables'] && ($minC == -1 || $v['count'] < $minC)){
            $minC = $v['count'];
            $waiterId = $k;
        }
    }

    if ($minC == -1) {
        echo 'No waiter available'; die;
    }

    //Insert Customer if exist
    $sqlCustomers = "INSERT INTO customers (name, surname, phone_number, mail) VALUES ('".$name."','".$surname."','".$phone."','".$email."')";
    $insertCustomer = $conn->query($sqlCustomers);

    $customerSelect = "SELECT c.id AS customers FROM customers AS c
                            WHERE c.mail = '".$email."'";

    $customerQuery= $conn->query($customerSelect);
    $customerId = $customerQuery->fetch_assoc()['customers'];

    if (empty($customerId)){
        echo 'Customer does not exist'; die;
    }

    //Check if Customer book a table during the day
    $customerAlreadyBookedQuery = "SELECT bt.id FROM booking_tables AS bt WHERE bt.id_customer= '".$customerId."' AND bt.serving_date = '".$date."'";
    $customerIdAlreadyBooked = $conn->query($customerAlreadyBookedQuery);

    while($row = $customerIdAlreadyBooked->fetch_assoc()) {
        $customerAlreadyBooked[] = $row;
    }

    if (!empty($customerAlreadyBooked)){
        echo 'Customer already booked for this day'; die;
    }

    //Select Table that is occupied
    $tableBusied = "SELECT bt.id_table AS id_table FROM booking_tables AS bt
                        LEFT JOIN employees AS e ON e.id = bt.id_employee
                        LEFT JOIN restaurants AS r ON e.id_restaurant = r.id
                        WHERE r.id = ".$restaurantId." AND bt.serving_date = '".$date."'
                        AND bt.serving_time <= '".$timeMax."' AND bt.serving_time >= '".$timeMin."'";

    $resultTableBusied = $conn->query($tableBusied);
    $tableBusiedArray = [];
    while($row = $resultTableBusied->fetch_assoc()) {
        $tableBusiedArray[]=$row['id_table'];
    }

    //Select Table free
    if(count($tableBusiedArray) >= 1) {
        $tableFree = "SELECT t.id AS id_table FROM tables AS t
                    WHERE t.id_restaurant = '".$restaurantId."'
                    AND t.max_seats >= '".$numbeSeats."'
                    AND t.id NOT IN (".implode(',',$tableBusiedArray).")";
    } else {
        $tableFree = "SELECT t.id AS id_table FROM tables AS t
                    WHERE t.id_restaurant = '".$restaurantId."'
                    AND t.max_seats >= '".$numbeSeats."'";
    }

    $tableFreeQuery = $conn->query($tableFree);
    $tableFreeId = $tableFreeQuery->fetch_assoc()['id_table'];

    //Insert the booking in booking_tables
    if(!empty($customerId) && !empty($tableFreeId) && !empty($waiterId) && !empty($date) && !empty($time)){
        $booking= "INSERT INTO booking_tables (id_customer, id_table, id_employee, serving_date, serving_time) 
              VALUES ('".$customerId."','".$tableFreeId."','".$waiterId."','".$date."','".$time."')";
        $bookingQuery = $conn->query($booking);
        echo 'Table booked !!';
    } elseif(empty($tableFreeId)) {
            echo 'No table available!'; die;
    } else {
        echo 'something wrong'; die;
    }
}
