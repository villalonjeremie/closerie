<?php
/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

fscanf(STDIN, "%d",
    $N
);

$tree = [];
$marque = [];
$inputTree = [];

for ($i = 0; $i < $N; $i++)
{
    $room = stream_get_line(STDIN, 256 + 1, "\n");
    $node = explode(' ', $room);
    $inputTree[] = $node;
}

function exploreTree($root, &$inputTree, $level, &$marque, &$tree) {
    $marque[] = $root;
    $tree[$level][] = $inputTree[$root][1];

    if ($inputTree[$root][2] == 'E' && $inputTree[$root][3] != 'E') {
        $inputTree[$root][2] = $inputTree[$root][3];
        $inputTree[$root][3] = 'E';
    }

    if ($inputTree[$root][2] == 'E') {
        return;
    }

    if (!in_array($inputTree[$root][2], $marque)) {
        exploreTree($inputTree[$root][2], $inputTree, $level+1, $marque, $tree);
    }

    if ($inputTree[$root][3] == 'E') {
        return;
    }

    if (!in_array($inputTree[$root][3], $marque)) {
        exploreTree($inputTree[$root][3], $inputTree, $level+1, $marque, $tree);
    }

    return;
};

exploreTree(0, $inputTree, 0, $marque, $tree);

$levels = count($tree);

while($levels > 1) {
    $i = 0;
    $temp = [];
    foreach($tree[$levels-2] as $el) {
        $temp[] = count($tree[$levels-1]) > 1 ?
            max($el + $tree[$levels-1][$i], $el + $tree[$levels-1][$i+1]) :
            $el + $tree[$levels-1][$i];
        $i++;
    }

    array_pop($tree);
    array_pop($tree);
    array_push($tree, $temp);
    $levels--;
}

echo ($tree[0][0]);