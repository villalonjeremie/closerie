
<html>
<head>
    <title>Admin Add Employee</title>
</head>
<body>
<form action="admin_add_employee.php" method="POST">
    Id Restaurant:
    <select id="id-restaurants" name="id_restaurant">
        <option selected hidden>Choose here</option>
        <option value=1>Nantes</option>
        <option value=2>Paris</option>
    </select>
    <br/>
    Name: <input type="text" name="name">
    <br/>
    Surname: <input type="text" name="surname">
    <br/>
    Birthday Date (1993-04-10): <input type="date" name="birthday_date">
    <br/>
    Employee Date (2019-04-10) : <input type="date" name="employee_date">
    <br/>
    Contrat Status :
    <select id="contrat-status" name="employment_contract_type" value="1">
        <option selected hidden>Choose here</option>
        <option value=1>Plein Temps</option>
        <option value=2>Mi Temps</option>
    </select>
    Employee Position :
    <select id="employee-position" name="position">
        <option selected hidden>Choose here</option>
        <option value=1>Serveur</option>
        <option value=2>Barman</option>
        <option value=3>Runner</option>
        <option value=4>Chef</option>
        <option value=5>Cuisinier</option>
        <option value=6>Assistant cuisinier</option>
    </select>
    <br/>
    Max Tables: <input type="number" name="max_tables">
    <br/>
    Lundi: <input type="checkbox" name="monday">
    <br/>
    Mardi: <input type="checkbox" name="tuesday">
    <br/>
    Mercredi: <input type="checkbox" name="wednesday">
    <br/>
    Jeudi: <input type="checkbox" name="thursday">
    <br/>
    Vendredi: <input type="checkbox" name="friday">
    <br/>
    Samedi: <input type="checkbox" name="saturday">
    <br/>
    Dimanche: <input type="checkbox" name="sunday">
    <br/>
    <input type="submit" name="submit" value="Ajout Employee">
</form>
<form action="admin_add_employee.php" method="POST">
    Id: <input type="text" name="id">
    <br/>
    <input type="submit" name="submit2" value="Supprimer ligne">
</form>
</body>
</html>


<?php
require_once("config.php");




if(isset($_POST["submit"]) && isset($_POST['id_restaurant']) && isset($_POST['id_restaurant']) && isset($_POST['name']) && isset($_POST['surname']) && isset($_POST['birthday_date']) && isset($_POST['employee_date']) && isset($_POST['employment_contract_type']) && isset($_POST['position']) && isset($_POST['max_tables'])){
    $idRestaurant = $conn->real_escape_string($_POST['id_restaurant']);
    $name = $conn->real_escape_string($_POST['name']);
    $surname = $conn->real_escape_string($_POST['surname']);
    $birthdayDate = $conn->real_escape_string($_POST['birthday_date']);
    $employeeDate = $conn->real_escape_string($_POST['employee_date']);
    $contractStatus = $conn->real_escape_string($_POST['employment_contract_type']);
    $employeePosition = $conn->real_escape_string($_POST['position']);
    $maxTables = $conn->real_escape_string($_POST['max_tables']);
    $monday = isset($_POST['monday']) ? 1:0;
    $tuesday = isset($_POST['tuesday']) ? 1:0;
    $wednesday = isset($_POST['wednesday']) ? 1:0;
    $thursday = isset($_POST['thursday']) ? 1:0;
    $friday = isset($_POST['friday']) ? 1:0;
    $saturday = isset($_POST['saturday']) ? 1:0;
    $sunday = isset($_POST['sunday']) ? 1:0;
    $sqlMeals = "INSERT INTO employees (id_restaurant, name, surname, birthday_date, employee_date, contract_status, employee_position, max_tables, monday, tuesday, wednesday, thursday, friday, saturday, sunday) VALUES 
                  ('".$idRestaurant."','".$name."','".$surname."','".$birthdayDate."','".$employeeDate."','".$contractStatus."','".$employeePosition."','".$maxTables."','".$monday."','".$tuesday."','".$wednesday."','".$thursday."','".$friday."','".$saturday."','".$sunday."')";
    $conn->query($sqlMeals);
}

if(isset($_POST["submit2"]) && isset($_POST['id'])){
    $id = $conn->real_escape_string($_POST['id']);
    $delete = "DELETE FROM employees WHERE employees.id = '".$id."'";
    $rows = $conn->query($delete);
}

$result = "SELECT e.id, r.town, e.name, e.surname, e.birthday_date, e.employee_date, ec.employment_contract_type, p.position,e.max_tables, e.monday, e.tuesday, e.wednesday, e.thursday, e.friday, e.saturday, e.sunday FROM employees AS e 
LEFT JOIN restaurants AS r ON r.id=e.id_restaurant
LEFT JOIN employment_contract AS ec ON ec.id=e.contract_status
LEFT JOIN positions AS p ON p.id=e.employee_position  ORDER BY e.id ASC";

$rows = $conn->query($result);
echo "<a href=\"index.html\" class=\"template-btn\">Retour Site</a>";
echo "<table border='1'>
<tr>
<th>Id</th>
<th>Restaurant</th>
<th>Name</th>
<th>Surname</th>
<th>Birthday Date</th>
<th>Employee Date</th>
<th>Contract Status</th>
<th>Employee Position</th>
<th>Max Tables</th>
<th>Monday</th>
<th>Tuesday</th>
<th>Wednesday</th>
<th>Thursday</th>
<th>Friday</th>
<th>Saturday</th>
<th>Sunday</th>
</tr>";

while($row = $rows->fetch_assoc())
{
    echo "<tr>";
    echo "<td>" . $row['id'] . "</td>";
    echo "<td>" . $row['town'] . "</td>";
    echo "<td>" . $row['name'] . "</td>";
    echo "<td>" . $row['surname'] . "</td>";
    echo "<td>" . $row['birthday_date'] . "</td>";
    echo "<td>" . $row['employee_date'] . "</td>";
    echo "<td>" . $row['employment_contract_type'] . "</td>";
    echo "<td>" . $row['position'] . "</td>";
    echo "<td>" . $row['max_tables'] . "</td>";
    echo "<td>" . $row['monday'] . "</td>";
    echo "<td>" . $row['tuesday'] . "</td>";
    echo "<td>" . $row['wednesday'] . "</td>";
    echo "<td>" . $row['thursday'] . "</td>";
    echo "<td>" . $row['friday'] . "</td>";
    echo "<td>" . $row['saturday'] . "</td>";
    echo "<td>" . $row['sunday'] . "</td>";
    echo "</tr>";
}
echo "</table>";
?>
